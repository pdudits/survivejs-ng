import {Component, ChangeDetectionStrategy} from '@angular/core';
import {LanesComponent} from './kanban/lanes.component';
import {Store, Lane, LaneActions} from './model';
import {Observable} from './rx.barrel';
import {LoadLanesComponent} from "./kanban/load-lanes.component.ts";
import {FetchStatusComponent} from "./kanban/fetch-status.component";
 
@Component({
    selector: 'app',
    template: `
        <div><load-lanes></load-lanes></div>
        <div><fetch-status></fetch-status></div>
        <div><button (click)="addLane()" class="add-lane">+</button>
        <lanes [lanes]="lanes | async"></lanes></div>`,
    directives: [LanesComponent, LoadLanesComponent, FetchStatusComponent],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
    private lanes:Observable<Lane[]>; 
    
    constructor(private store:Store, private actions:LaneActions) {
        this.lanes = this.store.map(s => s.lanes);
    }
       
    addLane() {
        this.actions.createLane('A Lane');
    }
    
}