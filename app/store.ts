import {Dispatcher,Store,State} from './model';
import {Action, asStoreReducer, LaneReducer, NotesReducer, FetchStateReducer, init} from './model/reducers'

import {Observable, Subject} from './rx.barrel';
import {provide} from '@angular/core';

function rootReducer(initState: State, action: Action<any>) {
    const notes = asStoreReducer(NotesReducer);
    const lanes = asStoreReducer(LaneReducer);
    const fetchStatus = asStoreReducer(FetchStateReducer);
    // TODO: a shallow comparison would be fine to return same value if there was no change
    return {
        notes: notes(initState.notes, action),
        lanes: lanes(initState.lanes, action),
        fetchState: fetchStatus(initState.fetchState, action)
    }
}

function stateFn(initState: State, actions: Observable<Action<any>>): Observable<State> {
    return actions.scan(rootReducer, initState);
}

const storageKey = "state";
function readStorage() {
    try {
      return JSON.parse(localStorage.getItem(storageKey));
    }
    catch(e) {
      return null;
    }
}

const initState:State = {
    notes: {},
    lanes: [],
    fetchState: {loading:false, errorDisplayed: true, error:null}
};

function writeStorage(state:Object) {
    console.log("Storing state", state);
    localStorage.setItem(storageKey, JSON.stringify(state))
}

export const store = [
    provide(Dispatcher, {useValue: new Subject<Action<any>>(null)}),
    provide(Store, {
        useFactory: (dispatcher: Dispatcher) => {
            const initial = readStorage() || initState;
            // we share so that reduction would not happen for every subscriber
            const store = stateFn(initial, dispatcher).share();
            
            // every subscriber will get the last known value
            const publish = store.publishBehavior(null);
            // start the publish subscription
            publish.connect();
            
            // we persist our state to storage (optional)
                        
            store.subscribe(writeStorage);
            
            // and trigger init action with first subscription that will override that null
            dispatcher.next(init({}));
            return publish;
        },
        deps: [Dispatcher]})];
