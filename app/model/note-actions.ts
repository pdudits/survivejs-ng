import {Injectable} from '@angular/core';
import {Note} from './model'
import * as a from './actions'

@Injectable()
export class NoteActions {
    
    constructor(private dispatch:a.Dispatcher) { }
    
    editNote(n:Note) {
        this.dispatch.next(a.editNote(n));
    }
    
    addNote(text:string) {
        console.log("Dispatching add",text);
        this.dispatch.next(a.addNote({task: text}));
    }
    
    deleteNote(id:string) {
        this.dispatch.next(a.deleteNote({id}));
    }
}

// actually a function, but with this notation we can verify the
// return value correctnes here as well as type of s.
export const NotesReducer:a.ActionReducer<{[key:string]: Note}> = function(s) { 
    return {
        addNote(p:typeof a.addNote.payload) {
            console.log("Reducer adding",p);
            const note = {
                id: p.tempId, task: p.task};
            var copy = Object.assign({},s);
            copy[note.id] = note;
            return copy;
        },
        
        editNote(p: typeof a.editNote.payload) {
            // es6 shim needs to be referred to support Object.assign I suppose
            const update = {};
            update[p.id] = p;
            return Object.assign({},s,update);
        },
        
        deleteNote(p: typeof a.deleteNote.payload) {
            var copy = Object.assign({},s);
            delete copy[p.id];
            return copy;
        }
    };
};


