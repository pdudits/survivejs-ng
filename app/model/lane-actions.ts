import {Injectable} from '@angular/core';
import {Lane} from './model';
import * as a from './actions';
import * as uuid from 'node-uuid';

@Injectable()
export class LaneActions {
    constructor(private dispatch:a.Dispatcher) {}
    
    createLane(name:string) {
        this.dispatch.next(a.createLane({name}));
    }
    
    addNote(task:string, lane:Lane) {
        this.dispatch.next(a.addNote({task, laneId:lane.id}));
    }
    
    deleteLane(id:string) {
        this.dispatch.next(a.deleteLane({id}));
    }
    
    editLaneName(id:string, name:string) {
        this.dispatch.next(a.editLaneName({id, name}));
    }
    
    moveNoteToLane(noteId:string, targetLane:string, successorId:string) {
        this.dispatch.next(a.moveNote({noteId, targetLane, successorId}));
    }
}

class LaneReducerM implements a.ReduceMethods<Lane[]> {
    
    constructor(private s:Lane[]) {}
    
    createLane(p: typeof a.createLane.payload) {
        const lane = {
            id: uuid.v4(),
            name: p.name,
            notes: []
        };
        const s = this.s || [];
        return s.concat(lane);
    }
    
    addNote(p: typeof a.addNote.payload) {
        if (p.laneId) {
            return this.attachToLane(p.laneId, p.tempId);
        } else {
            return this.s;
        }
    }

    lanesLoaded(lanes:Lane[]) {
        return lanes;
    }
    
    deleteLane(p: {id:string}) {
        return this.s.filter(lane => lane.id !== p.id);
    }
    
    editLaneName(p: {id:string, name:string}) {
        return this.s.map(lane=> lane.id === p.id ? Object.assign({}, lane, {name: p.name}) : lane);
    }
    
    deleteNote(p: typeof a.deleteNote.payload) {
        return this.s.map(lane => lane.notes.indexOf(p.id) >= 0 
            ? Object.assign({}, lane, { notes: lane.notes.filter( n => n !== p.id) })
            : lane);
    }
    
    moveNote(p: typeof a.moveNote.payload) {
        return this.s.map(lane => {
            var n = lane.notes;
            const oldIdx = lane.notes.indexOf(p.noteId);
            if (oldIdx >= 0) {
                n = [...n.slice(0, oldIdx), ...n.slice(oldIdx + 1, n.length)]
            }
            if (lane.id === p.targetLane) {
                var newIdx = n.indexOf(p.successorId);
                if (newIdx < 0) {
                    newIdx = n.length;
                }
                n = [...n.slice(0, newIdx), p.noteId, ...n.slice(newIdx, n.length)];
            }
            if (n !== lane.notes) {
                return Object.assign({},lane,{notes:n});
            } else {
                return lane;
            }
        });
    }
      
    private attachToLane(laneId:string, noteId:string) {
        return this.s.map(lane => {
            if(lane.id === laneId) {
              if (lane.notes.indexOf(noteId) >= 0) {
                console.warn('Already attached note to lane', lane);
              } else {
                console.log("Attaching note",noteId);
                lane.notes.push(noteId);
              }
            }
            return lane;
        });
    }
    
    private detachFromLane(laneId:string, noteId:string) {
        return this.s.map(lane => {
            if(lane.id === laneId) {
                lane.notes = lane.notes.filter(note => note !== noteId);
            }
            return lane;
        });
    }
}

export const LaneReducer: a.ActionReducer<Lane[]> = s => new LaneReducerM(s);