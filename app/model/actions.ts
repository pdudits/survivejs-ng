import {Note, State, Lane} from "./model";
import * as uuid from "node-uuid";

import {Observable, Subject} from '../rx.barrel'

export interface Action<P> {
    type:string,
    payload:P
}

interface ActionCreator<I,P> {
  /**
   * contant representing unique type
   */
  type: string;
  (payload: I): Action<P>;
  
  /**
   * This property is not actually safe, just allows for getting the type of payload via typeof.
   * But payloads probably need to have their own types.
   */
  payload: P;  
}

function actionCreator<P>(type: string): ActionCreator<P,P> {
  return Object.assign(
    (payload: P) => ({type, payload}),
    {type}
  );
}

function transformingActionCreator<I,P>(type: string, creator: (I) => P): ActionCreator<I,P> {
    return Object.assign(
        (input: I) => ({type, payload: creator(input)}),
        {type}
    );
}

export function isType<I,P>(action: Action<any>, actionCreator: ActionCreator<I,P>): action is Action<P> {
  return action.type === actionCreator.type;
}

// application defines what the action payloads are, therefore it should be able to extend them
// without affecting reducers.
type Status = "started"|"finished"|"error";

export const init = actionCreator<{}>("init");
export const editNote = actionCreator<Note>("editNote");
export const deleteNote = actionCreator<{id:string}>("deleteNote");
export const deleteLane = actionCreator<{id:string}>("deleteLane");
export const editLaneName = actionCreator<{id:string, name:string}>("editLaneName");
export const lanesLoaded = actionCreator<Lane[]>("lanesLoaded");
export const dataFetch = actionCreator<{state:Status, error?:any}>("dataFetch");
export const moveNote = actionCreator<{noteId:string, targetLane:string, successorId:string}>("moveNote");

// so if action creator creates remote object, it might need a temporary id first, that will change
// after it is commited. Also, the input to the action creator differs from action payload now.
interface AddNoteInput {task:string, laneId?:string}
interface AddNotePayload extends AddNoteInput {
    tempId: string
} 

// TODO: We will need to support some async creator, Observable<Action> I suppose
export const addNote = transformingActionCreator<AddNoteInput, AddNotePayload>("addNote",
    (i) => (Object.assign(i, { tempId: uuid.v4()})));
    
export const createLane = actionCreator<{name:string}>("createLane");

export interface ReduceMethods<S> {
    addNote?: (p: typeof addNote.payload) => S;
    editNote?: (p: typeof editNote.payload) => S;
    deleteNote?: (p: typeof deleteNote.payload) => S;
    createLane?: (p: typeof createLane.payload) => S;
    deleteLane?: (p: typeof deleteLane.payload) => S;
    editLaneName?: (p: typeof editLaneName.payload) => S;
    //backend actions
    dataFetch?: (p: typeof dataFetch.payload) => S;
    lanesLoaded?: (p: typeof lanesLoaded.payload) => S;
    moveNote?: (p: typeof moveNote.payload) => S;
}

export type ActionReducer<S> = (state: S) => ReduceMethods<S>;

type Reducer<S> = (initState:S, a:Action<any>) => S;

export function asStoreReducer<S>(rf:ActionReducer<S>):Reducer<S> {
    return (state:S,a:Action<any>):S => {
        const reducers = rf(state);
        // this would be typesafe approach, that compiles fine
        // if (isType(a, addNote)) {
        //    return reducers.addNote(a.payload);
        // } else if (isType(a, deleteNote)) {
        //    return reducers.deleteNote(a.payload);
        // } else if (isType(a, deleteLane)) {
        //     return reducers.deleteLane(a.payload);
        // } 
        // type unsafe approach is more general
        const key = a.type;
        if (reducers[key] != undefined) {
            return reducers[key](a.payload);
        } else {
            //console.warn("Reducer does not support action",key);
            return state;
        }
    }
}

export class Dispatcher extends Subject<Action<any>> {}

export class Store extends Observable<State> {} 