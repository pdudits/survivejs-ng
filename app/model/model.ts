export type NoteId = string

export type Note = {
    id:NoteId;
    task:string;
}

export type Lane = {
    id:string;
    name:string;
    notes:NoteId[];
}

export type FetchState = {
    loading: boolean;
    error: any;
    errorDisplayed: boolean;
}

export interface State {
    notes: {[key: string] : Note};
    lanes: Lane[];
    fetchState: FetchState;
} 
