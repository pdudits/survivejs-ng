import * as a from './actions';
import {Injectable} from "@angular/core";
import {Http, Response} from '@angular/http';
import {Lane, FetchState} from "./model";
import {dataFetch} from "./actions";

@Injectable()
export class BackendConnectionService {
    constructor(private http:Http, private dispatch:a.Dispatcher) {
    }

    loadLanes() {
        this.dispatch.next(a.dataFetch({state: "started"}));
        this.http.request('http://56f29c98edb0a5110068439c.mockapi.io/api/lanes')
            .subscribe((res:Response) => {
                const data:Lane[] = res.json();
                this.dispatch.next(a.lanesLoaded(data));
            }, (e)=> {
                console.log(e);
                this.dispatch.next(a.dataFetch({state: "error",error:e}));
            }, ()=> {
                this.dispatch.next(a.dataFetch({state: "finished"}));
            });

    }
}

export const FetchStateReducer:a.ActionReducer<FetchState> = function (s) {
    return {
        dataFetch(p: typeof dataFetch.payload) {
            switch (p.state) {
                case "started":
                    return Object.assign({},s,{loading: true});
                case "finished":
                    return Object.assign({},s,{loading: false});
                case "error":
                    return {error:p.error, errorDisplayed: true, loading: false};
            }
            return s;
        }  
    };
};

