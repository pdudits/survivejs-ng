/*
 * Angular
 */
import {Component} from '@angular/core';
import {Store} from "../model/actions";
import {Observable} from "../rx.barrel";

@Component({
    selector: 'fetch-status',
    template: `<div *ngIf="(loading$|async)" style="width: 100%; height: 100%; background: gray;">loading...</div>`
})
export class FetchStatusComponent {
    loading$:Observable<boolean>;

    constructor(private s:Store) {
        this.loading$ = s.map(state => state.fetchState.loading);
    }

}