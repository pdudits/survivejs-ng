import {Directive, Input, ElementRef} from '@angular/core';

@Directive({
    selector: "[dnd-container-id]"
})
export class DndContainerIdDirective {
    @Input('dnd-container-id') id;
    
    constructor(public element:ElementRef) { }
}


