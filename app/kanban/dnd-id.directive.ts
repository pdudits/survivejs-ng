import {Directive, Input, ElementRef} from '@angular/core';

@Directive({
    selector: "[dnd-id]"
})
export class DndIdDirective {
    @Input('dnd-id') id;
    
    constructor(public element:ElementRef) {    }
    
    findModel(element:Element) {
        if (element === this.element.nativeElement) {
            return this.id;
        } else {
            return null;
        }
    }
}


