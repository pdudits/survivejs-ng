import {Component, Input, Output, EventEmitter} from '@angular/core';
import {NoteComponent} from './note.component';
import * as m from '../model/model';
import {DndContainerDirective} from './dnd-container.directive'
import {DndIdDirective} from './dnd-id.directive'

@Component({
    selector: 'notes',
    template: `<ul class="notes" dnd-container>
        <li *ngFor="#n of notes" class="note" [dnd-id]="n.id">
          <note [task]="n.task"
            (editFinished)="edit(n.id, $event)"
            (delete)="onDelete(n.id)">
          </note></li>
      </ul>`,
    directives: [NoteComponent, DndContainerDirective, DndIdDirective]
})
export class NotesComponent {
    @Input() notes : m.Note[];
    
    @Output() editFinished = new EventEmitter<m.Note>();
    @Output() delete = new EventEmitter<String>();
    
    edit(id:string, text:string) {
        console.log("Notes.edit", id);
        this.editFinished.emit({id:id, task:text});
    }
    
    onDelete(id:string) {
        this.delete.emit(id);
    }
}