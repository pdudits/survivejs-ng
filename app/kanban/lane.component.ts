import {Component, Input, OnInit} from '@angular/core';
import {Lane, Note, Store, LaneActions, NoteActions} from '../model';
import {NotesComponent} from './notes.component'
import {Observable} from '../rx.barrel'
import {DndContainerIdDirective} from './dnd-container-id.directive';



@Component({
    selector: `lane`,
    template: `<div [class]="clazz">
        <div class="lane-header">
          <div class="lane-add-note">
            <button (click)="addNote()">+</button>
          </div>
          <div class="lane-name">
            <input #in type="text" [value]="lane.name"
                 (blur)="finish(in.value)" (keyup.Enter)="finish(in.value)" (keyup.Escape)="finish(lane.name)">
             <button (click) = "deleteLane()" class="lane-delete">x</button>
           </div>
        </div>
           <notes [notes]="notes | async"
              (editFinished)="edit($event)"
              (delete)="onDelete($event)" [dnd-container-id]="lane.id"></notes>
      </div>`,
    directives: [NotesComponent, DndContainerIdDirective]
})
export class LaneComponent implements OnInit {
    @Input() clazz;
    @Input() lane: Lane;
    notes: Observable<Note[]>;   

    constructor(private store:Store, private laneActions:LaneActions,
                private noteActions:NoteActions) {
    }

    ngOnInit() {
        // we cannot access @Input attributes sooner than ngOnInit
        // in map, we explicitly bind filternotes method to this, because the reference
        // otherwise gets lost
        this.notes = this.store.map(s => s.notes).map(notes => this.lane.notes.map(id => notes[id]));
    }

    finish(name:string) {
        this.laneActions.editLaneName(this.lane.id, name);
    }

    addNote() {
        this.laneActions.addNote('New task',this.lane);
    }

    edit(edit:Note) {
        this.noteActions.editNote(edit);
    }

    onDelete(id:string) {
        this.noteActions.deleteNote(id);
    }

    deleteLane() {
        this.laneActions.deleteLane(this.lane.id);
    }
}
