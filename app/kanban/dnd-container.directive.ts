import {Directive, ContentChildren, ElementRef, Optional, QueryList} from '@angular/core';
import {DndContainerIdDirective} from './dnd-container-id.directive';
import {DndIdDirective} from './dnd-id.directive';
import {DragAndDropDirective, Container} from './drag-and-drop.directive'

@Directive({
    selector: "[dnd-container]"
})
export class DndContainerDirective implements Container {
    
    // we can see the children of directive as long as they are in single template
    @ContentChildren(DndIdDirective) modelIds: QueryList<DndIdDirective>
    
    // and we can see our parents, as they are all injected
    // what we cannot do is to look inside child templates.
    constructor(public element:ElementRef, @Optional() private id:DndContainerIdDirective,
         private dnd:DragAndDropDirective) {
    }
    
    get model() {
        return this.id ? this.id.id : "";
    }
    
    ngOnInit() {
        this.dnd.registerContainer(this);
    }
    
    ngOnDestroy() {
        this.dnd.unregisterContainer(this);
    }
    
    findItemModel(element):any {
        var item = this.modelIds.toArray().find(m => m.element.nativeElement === element);
        return item ? item.id : undefined;
    }
    
    matchesElement(element) {
        return element === this.element.nativeElement;
    }
    
}


