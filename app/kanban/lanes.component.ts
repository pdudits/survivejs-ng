import {Component, Input, ViewEncapsulation} from '@angular/core';
import * as m from '../model/model';
import {LaneComponent} from './lane.component';
import {LaneActions} from '../model';
import {DragAndDropDirective, DropEvent} from './drag-and-drop.directive';

@Component({
    selector: `lanes`,
    styles: [require('raw!dragula/dist/dragula.min.css')],
    encapsulation: ViewEncapsulation.None,
    template: `<div class="lanes" (drag-and-drop)="moveItems($event)">
      <lane *ngFor="#lane of lanes" [lane]="lane" class="lane"></lane>
    </div>`,
    directives: [LaneComponent, DragAndDropDirective]
})
export class LanesComponent {
    @Input() lanes: m.Lane[];

    constructor(private laneActions: LaneActions) {
    }
    
    moveItems(e:DropEvent<string,string>) {
        this.laneActions.moveNoteToLane(e.item, e.targetContainer, e.successor);
    }

}