import {Directive, Output, EventEmitter, ElementRef} from '@angular/core';
import * as dragula from 'dragula';

export type DropEvent<I,C> = {
    item:I, 
    sourceContainer:C,
    targetContainer:C,
    successor?:I
}

@Directive({
    selector: "[drag-and-drop]"
})
export class DragAndDropDirective {
    @Output('drag-and-drop') dropEvent = new EventEmitter<DropEvent<any,any>>();
    private drake:dragula.Drake;
    
    private containers: Container[] = [];
    private containersDirty = false;
       
    constructor() {}
    
    registerContainer(c:Container) {
        this.containers.push(c);
        this.containersDirty = true;
    }
    
    unregisterContainer(c:Container) {
        this.containers = this.containers.filter(cont => cont !== c);
        this.containersDirty = true;
    }
    
    ngAfterContentChecked() {
        if (this.containersDirty) {
            this.createDrake();
        }
    }
    
    private createDrake() {
        console.log("containers changed to", this.containers);
        var elements = this.containers.map(dir => dir.element.nativeElement);
        if (this.drake) {
            this.drake.destroy();
        }
        this.drake = dragula(elements);
        this.containersDirty = false;
        
        this.drake.on('drop', this.drop.bind(this))
    }
    
    findItemModel(el:Element) {
        var m = this.containers.reduce((val, c) => val !== undefined ? val : c.findItemModel(el), undefined); 
        console.log("found item model",m)
        return m;
    }
    
    findContainerModel(el:Element) {
        var c = this.containers.find(c => c.matchesElement(el));
        return c ? c.model : null;
    }
    
    drop(el: Element, target: Element, source: Element, sibling?: Element) {
        var itemModel = this.findItemModel(el);
        var siblingModel = sibling ? this.findItemModel(sibling) : undefined;
        var targetModel = this.findContainerModel(target);
        var sourceModel = this.findContainerModel(source);
        console.log("Drop item",itemModel,"to",targetModel,"from",sourceModel,"next to",siblingModel);
        this.dropEvent.emit({
            item: itemModel,
            sourceContainer: sourceModel,
            targetContainer: targetModel,
            successor: siblingModel
        });
    }    
    
}

export interface Container {
    model: any;
    element: ElementRef;
    findItemModel(element):any;
    matchesElement(element):boolean;
}

