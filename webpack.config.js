var path = require('path');
var merge = require('webpack-merge');
var webpack = require('webpack');

var TARGET = process.env.npm_lifecycle_event;
var package = require('./package.json');

var PATHS = {
  app: root('app/'),
  build: root('target/build')
};

function root(p) {
    return path.join(__dirname,p);
}

var common = {
  // Entry accepts a path or an object of entries. We'll be using the
  // latter form given it's convenient with more complex configurations.
  entry: {
    vendor: ['es7-reflect-metadata/dist/browser',
        // without the pollyfils component updates are not wokring
        'es6-shim',
        'zone.js',
        // angular2 entry point
        '@angular/platform-browser-dynamic',
        './app/rx.barrel',
        '@angular/http',
        'node-uuid', 'dragula'],
        //automated approach doesn't work as good for angular
//            Object.keys(package.dependencies).filter(function(v) {
//        return v !=='angular2' && v !== 'es6-shim' && v !== 'es7-reflect-metadata';
//      }).concat('@angular/bundles/angular2-polyfills', 
//        '@angular/platform/browser', 
//        '@angular/http',
//        'es7-reflect-metadata/dist/browser'),  
    app: path.join(PATHS.app, 'index.ts'),
    //experiment: path.join(PATHS.app, 'experiment/index.ts')
  },
  resolve: {
    // ensure loader extensions match. empty string is essential!
    extensions: ["", ".webpack.js", ".web.js", ".js",'.ts', '.css']
  },
  module: {
      loaders: [
          { test: /\.ts$/, loader: 'ts-loader', include: PATHS.app },
          { test: /\.css$/, loaders: ['style', 'css'], include: PATHS.app }
      ],
      preLoaders: [
          { test: /\.js$/, loader: "source-map-loader", exclude: [ root('node_modules/rxjs') ] }
      ]      
  },
  
  output: {
    path: PATHS.build,
    publicPath: 'bundle/',
    filename: '[name].js',
    sourceMapFilename: '[name].map',
    chunkFilename: '[id].chunk.js'    
  },
  
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({ names: ['vendor', 'manifest']}),   
    //new webpack.PrefetchPlugin(root('.'), "angular2/common"),
 //   new webpack.PrefetchPlugin(root('.'), "crypto-browserify/index"),
    //new webpack.PrefetchPlugin(root('.'), "dragula/dragula"),
    //new webpack.PrefetchPlugin(root('.'), "angular2/http"),
  ]
};

if(TARGET === 'start' || !TARGET) {
  module.exports = merge(common, {
    // custom configuration below
    //devtool: 'source-map', // survivejs says eval-source-map
    
    devServer: {
      contentBase: 'app',
      
      // Enable history API fallback so HTML5 History API based
      // routing works. This is a good default that will come
      // in handy in more complicated setups.
      historyApiFallback: true,
      hot: true,
      inline: true,
      progress: true,

      // Display only errors to reduce the amount of output.
      stats: 'errors-only',

      // Parse host and port from env so this is easy to customize.
      //
      // If you use Vagrant or Cloud9, set
      // host: process.env.HOST || '0.0.0.0';
      //
      // 0.0.0.0 is available to all network devices unlike default
      // localhost
      host: process.env.HOST,
      port: process.env.PORT || 3000
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new webpack.SourceMapDevToolPlugin ({
         exclude: ['vendor.js'], // source maps take lots of time
         module: true, // use SourceMaps from loaders 
         filename: "[name].map",
         })
    ]
  });      
}

if(TARGET === 'build') {
  module.exports = merge(common, {
    output: {
      path: PATHS.build,
      filename: '[name].[chunkhash].js',
      chunkFilename: '[chunkhash].js'
    },
    plugins: [
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        }
      }),
      new webpack.SourceMapDevToolPlugin ({
         module: true, // use SourceMaps from loaders 
         filename: "[name].map",
         })
    ]
  });
}
