import {Dispatcher,Store,State} from '../app/model';
import {store} from '../app/store';
import {ReflectiveInjector} from '@angular/core';
import {Scheduler} from 'rxjs/Rx';
import * as a from '../app/model/actions'

beforeEach(() => {
    localStorage.clear();
});

describe('store provider', () => {
    const injector = ReflectiveInjector.resolveAndCreate(store);
    it('should resolve Dispatcher', () => {
        expect(injector.get(Dispatcher)).toBeDefined();
    });
    
    it('should resolve Store', () => {
        expect(injector.get(Store)).toBeDefined(); 
    });
});

describe('store', () => {
    const injector = ReflectiveInjector.resolveAndCreate(store);
    const st:Store = injector.get(Store);
    const dispatcher = injector.get(Dispatcher);
    
    it('should provide a state upon subscription', () => {
        var initState:State = null;
        const o = st.take(1).observeOn(Scheduler.queue).subscribe(
          (state) => initState = state
        );
        expect(initState.notes).toEqual({});
    });
});

describe('notes reducer', () => {
    const injector = ReflectiveInjector.resolveAndCreate(store);
    const st:Store = injector.get(Store);
    const dispatcher:Dispatcher = injector.get(Dispatcher);
    it ('should add a note upon addNote', () => {
        var initState:State = null;
        
        st.take(2).observeOn(Scheduler.queue).subscribe(
            (state) => initState = state
        );
        dispatcher.next(a.addNote({task: 'Hello'}));
        
        const keys = Object.keys(initState.notes);
        expect(keys.length).toBe(1);
        expect(initState.notes[keys[0]].task).toBe("Hello");
    })
});