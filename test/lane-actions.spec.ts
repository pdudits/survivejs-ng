import {LaneReducer} from '../app/model/lane-actions';
import {Lane} from '../app/model';
import * as action from '../app/model/actions'

describe("LaneReducer on moveNote action",() => {
    it('should move to last position when sucessor is null', () => {
        const s:Lane[] = [{id: "1", name: "1", notes:["1","2","3"]}];
        const a = {noteId: "1", targetLane: "1", successorId: null};
        
        const r = LaneReducer(s).moveNote(a);
        expect(r[0].notes).toEqual(["2","3","1"]);
    })
    
})


